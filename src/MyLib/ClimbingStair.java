/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyLib;

/**
 *
 * @author BaoHa
 */
public class ClimbingStair {

    public static void main(String[] args) {
        System.out.println(climbingStairs(2));
    }

    public static int climbingStairs(int n) {
        int[] a = new int[n + 1];
        a[0] = 1;
        a[1] = 1;
        for (int i = 2; i < n; i++) {
            a[i] = a[i - 1] + a[i - 2];
        }
        int result = a[n];
        return result;
    }
}
