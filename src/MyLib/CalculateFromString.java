/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyLib;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author BaoHa
 */
public class CalculateFromString {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ScriptException {
        // TODO code application logic here
        ScriptEngineManager sem = new ScriptEngineManager();
        ScriptEngine se = sem.getEngineByName("JavaScript");
        String cal = "1+1+2*3*4*5+1+2/3";
        //<script> </script>
        System.out.println(se.eval(cal));
    }
    
}
