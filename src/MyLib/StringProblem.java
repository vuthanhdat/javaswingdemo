/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyLib;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.stream.IntStream;

/**
 *
 * @author BaoHa
 */
public class StringProblem {

    public static void main(String[] args) {
        String s = "abaceeaaaabadd";
        System.out.println(firstNotRepeatChar2(s));
    }

    public static char firstNotRepeatChar(String s) {
        char c = '_';
        boolean flag = true;
        for (int i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            for (int j = 0; j < s.length(); j++) {
                if (c == s.charAt(j) && i != j) {
                    flag = false;
                    break;
                } else if (i != j && c != s.charAt(j)) {
                    flag = true;
                }
            }
            if (flag) {
                return c;
            }
        }
        return '_';
    }

    public static char firstNotRepeatChar2(String s) {
        for (char i : s.toCharArray()) {
            if (s.indexOf(i) == s.lastIndexOf(i)) {
                return i;
            }
        }
        return '_';
    }

    boolean isCryptSolution(String[] crypt, char[][] solution) {
        HashMap<Character, Integer> a = new HashMap<Character, Integer>();
        for (int i = 0; i < solution.length; i++) {
            a.put(solution[i][0], solution[i][1] - '0');
        }
        String b = "", c = "", d = "";
        for (char x : crypt[0].toCharArray()) {
            b += a.get(x);
            if (b.length() > 1 && b.charAt(0) == '0') {
                return false;
            }
        }
        for (char x : crypt[1].toCharArray()) {
            c += a.get(x);
            if (c.length() > 1 && c.charAt(0) == '0') {
                return false;
            }
        }
        for (char x : crypt[2].toCharArray()) {
            d += a.get(x);
            if (d.length() > 1 && d.charAt(0) == '0') {
                return false;
            }
        }
        return Long.parseLong(d) == Long.parseLong(b) + Long.parseLong(c);
    }
}
