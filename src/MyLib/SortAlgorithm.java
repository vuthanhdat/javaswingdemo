/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyLib;

import static MyLib.SortAlgorithm.swapElement;
import java.util.HashMap;
import java.util.Random;

/**
 *
 * @author BaoHa
 */
public class SortAlgorithm {

    public static void main(String[] args) {
        //TODO: Test cound character in string
        String input = "hello world";
        countCharInString(input);

        //TODO: Test Sorting algorithm
        //int[] arr = {4, 10, 7, 11, 2, 19, 9, 2, 17, 11};
        //System.out.println(findSecondMaxElement(arr));
        //System.out.println(findSecondMinElement(arr));
        //int[] arr = randomArr(10, 20);
        //printArr(arr);
        //QuickSort q = new QuickSort();
        //q.sort(arr);
        //printArr(arr);
        //TODO: Test quicksort 
        //System.out.println(findMaxElement(arr));
        //System.out.println(findMinElement(arr));
        //insertion_srt(arr);
        //printArr(arr);
        //TODO: Check Prime number execute 
        //System.out.print(checkPrimeNumber(17));
    }

    //TODO: Swap array use for sorting
    public static void swapElement(int i, int j, int[] arr) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static boolean checkPrimeNumber(int number) {
        int boundary = (int) Math.sqrt(number);
        if (number == 1) {
            return false;
        }
        if (number == 2) {
            return true;
        } else {
            for (int i = 3; i < boundary; i++) {
                return number % i != 0;
            }
        }
        return true;
    }

    public static int findMinElement(int[] arr) {
        int min = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
            }
        }
        return min;
    }
    
    public static int findSecondMinElement(int[] arr){
        int min =findMinElement(arr);
        int min2=arr[0];
        for(int i=0;i<arr.length;i++){
            if(min2 >arr[i] && arr[i]>min){
                min2 = arr[i];
            }
        }
        
        return min2;
    }
    
    public static int findMaxElement(int[] arr) {
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        return max;
    }

    public static int findSecondMaxElement(int[] arr){
        int max= arr[0];
        int max2=0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]>max){
                max2 = max;
                max = arr[i];
            }
            else if(arr[i]>max2){
                max2 = arr[i];
            }
        }
        return max2;
    }

    
    public static int[] randomArr(int size, int limit) {
        Random r = new Random();
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = r.nextInt(limit);
        }
        return arr;
    }
    
    public static int randomElement(int limit){
        Random r = new Random();
        return r.nextInt(limit);
    }

    
    public static void printArr(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    //TODO: Bubble sort
    /*
     * Bubble sort has worst-case and average complexity both О(n2), where n is the number of items being sorted. There exist many sorting algorithms with substantially better worst-case or average complexity of O(n log n). Even other О(n2) sorting algorithms, such as insertion sort, tend to have better performance than bubble sort. Therefore, bubble sort is not a practical sorting algorithm when n is large.Performance of bubble sort over an already-sorted list (best-case) is O(n).
     */
    public static void bubble_srt(int[] arr) {
        int n = arr.length;
        int k;
        for (int i = n; i >= 0; i--) {
            for (int j = 0; j < n - 1; j++) {
                k = j + 1;
                if (arr[j] > arr[k]) {
                    swapElement(j, k, arr);
                    printArr(arr);
                }
            }
            //printArr(arr);
        }
    }
    
    

    public static void insertion_srt(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (arr[j] < arr[j - 1]) {
                    swapElement(j, j - 1, arr);
                    printArr(arr);
                }
            }
        }
    }

    public static void selection_srt(int[] arr) {

    }

    public static void countCharInString(String input) {
        HashMap<Character, Integer> charCount = new HashMap<>();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (!charCount.containsKey(c)) {
                charCount.put(c, 1);
            } else {
                charCount.put(c, charCount.get(c) + 1);
            }
        }

        System.out.print(charCount);
    }
}