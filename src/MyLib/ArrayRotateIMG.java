/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyLib;

/**
 *
 * @author BaoHa
 */
public class ArrayRotateIMG {

    public static void main(String[] args) {
        int[][] img = new int[][]{{1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}};
        int[][] img1 = new int[][]{{1}};
        int a[][] =rotateImage(img1);
        printArr(a);
    }

    public static int[][] rotateImage(int[][] a) {
        int temp[][] = new int[a.length][a[0].length];
        int row=0,col=0;
        for (int i = 0; i < a.length; i++) {
            for (int j = a[i].length - 1; j >= 0; j--) {
                temp[row][col]=a[j][i];
                //System.out.print(a[j][i]+ " ");
                col++;
            }
            //System.out.println();
            col=0;
            row++;
        }
        //System.out.println(row + " "+ col);
        return temp;
    }
    
    public static void printArr(int[][] a){
        for(int i=0;i<a.length;i++){
            for(int j=0;j<a[i].length;j++){
                System.out.print(a[i][j] +" ");
            }
            System.out.println();
        }
    }
}
