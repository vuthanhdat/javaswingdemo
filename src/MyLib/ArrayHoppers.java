/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyLib;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BaoHa
 */
public class ArrayHoppers {
    public static void main(String[] args) {
        int[] a1 = {2,3,1,1,4};
        System.out.println(getShortestJumps(a1));
        


        int[] a2 = {3, 1, 10, 1, 4};
        System.out.print(getShortestJumps(a2));
    }
    
    public static List<Integer> getShortestJumps01(int[] jump) {
    final List<Integer> list = new ArrayList<Integer>();
    if (jump.length == 0) {
        return list;
    }
    int cursor = 0;
    int best = 0;
    int range = 0;
    int remaining = 1;
    while (cursor + 1 < jump.length) {
        if (cursor + jump[cursor] > range) {
            // jumping from here would extend us further than other alternatives so far
            range = cursor + jump[cursor];
            best = cursor;
            if (range >= (jump.length - 1)) {
                // in fact, this jump would take us to the last member, we have a solution
                list.add(best);
                break;
            }
        }
        if (--remaining == 0) {
            // got to the end of our previous jump, move ahead by our best.
            list.add(best);
            remaining = range - cursor;
        }

        cursor++;
    }
    // always add the last member of the array
    list.add(jump.length - 1);
    return list;
}
    
    public static List<Integer> getShortestJumps(int[] jump) {
        final List<Integer> list = new ArrayList<Integer>();
        list.add(0);

        for (int i = 0; i < jump.length - 1; ) {
            int iSteps = Math.min(jump.length - 1, i + jump[i]);
            System.out.println(i);
            System.out.println(jump[i]);
            //System.out.println(i + jump[i]);
            System.out.println("steps reachable "+iSteps);// iSteps is all the consecutive steps reachable by jumping from i.
            int maxStep = Integer.MIN_VALUE; // max step is one of the step in iSteps, which has the max potential to take us forward.
            /*  trying each step of iSteps */
            
            for (int j = i + 1; j <= iSteps; j++) {

                /* being greedy and picking up the best step */
                if (maxStep < jump[j]) {
                    maxStep = j;
                }
            }
            System.out.println("Max step:"+ maxStep);
            list.add(maxStep);
            i = maxStep; // jump to the maxStep.
        }

        return list;
    }
}
